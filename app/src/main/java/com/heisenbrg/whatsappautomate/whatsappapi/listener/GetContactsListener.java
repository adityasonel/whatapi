package com.heisenbrg.whatsappautomate.whatsappapi.listener;

import com.heisenbrg.whatsappautomate.whatsappapi.model.WContact;

import java.util.List;

public interface GetContactsListener {
    void receiveWhatsappContacts(List<WContact> contacts);
}
