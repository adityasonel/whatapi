package com.heisenbrg.whatsappautomate.whatsappapi.listener;

import com.heisenbrg.whatsappautomate.whatsappapi.model.WContact;
import com.heisenbrg.whatsappautomate.whatsappapi.model.WMessage;

import java.util.List;

public interface SendMessageListener {
    void finishSendWMessage(List<WContact> contact, WMessage message);
}
