package com.heisenbrg.whatsappautomate;

import android.content.ComponentName;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.heisenbrg.whatsappautomate.whatsappapi.WhatsApi;
import com.heisenbrg.whatsappautomate.whatsappapi.exception.WhatsappNotInstalledException;
import com.heisenbrg.whatsappautomate.whatsappapi.listener.SendMessageListener;
import com.heisenbrg.whatsappautomate.whatsappapi.model.WContact;
import com.heisenbrg.whatsappautomate.whatsappapi.model.WMessage;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import eu.chainfire.libsuperuser.Shell;

public class MainActivity extends AppCompatActivity {

    private final String TAG = MainActivity.class.getSimpleName();

    //todo: - add your whatsapp message here...
    String message = "This is automated whatsapp message, number: ";

    BufferedReader reader;
    InputStream file;
    String line;

    List<WContact> wlist = new ArrayList<>();
    WMessage wmsg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Shell.SU.run("chmod 777 /data/data/com.whatsapp");

        wmsg = new WMessage(message, null, MainActivity.this);

        try{
            file = getAssets().open("data.txt");
            reader = new BufferedReader(new InputStreamReader(file));
            line = reader.readLine();

            while(line != null) {
                wlist.add(new WContact(line, line));
                line = reader.readLine();
            }
        } catch(IOException ioe){
            Log.d(TAG, "err, while reading data assest: - " + ioe.toString());
        }
    }

    public void onClickFab(View view) {
        Log.d(TAG, "whatsapi started...");
        try {
            WhatsApi.getInstance().sendMessage(wlist, wmsg, MainActivity.this, new SendMessageListener() {
                @Override
                public void finishSendWMessage(List<WContact> contact, WMessage message) {
                    Log.d(TAG, "whatsapi completed!");
                }
            });
        } catch (IOException | WhatsappNotInstalledException e) {
            Log.d(TAG, "err, while sending message: -" + e.toString());
        }
    }
}
